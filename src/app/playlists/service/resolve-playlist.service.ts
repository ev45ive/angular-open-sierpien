import { Injectable } from "@angular/core";
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { Playlist } from "src/app/models/Playlist";
import { Observable } from "rxjs";
import { PlaylistsService } from "../services/playlists.service";
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: "root"
})
export class ResolvePlaylistService implements Resolve<Playlist> {
  
  constructor(private service: PlaylistsService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Playlist | Observable<Playlist> | Promise<Playlist> {
    const selectedId = route.paramMap.get("playlist_id");
    return this.service.getPlaylist(selectedId).pipe(
      delay(2000)
    )
  }
}
