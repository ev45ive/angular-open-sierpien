import { TestBed } from '@angular/core/testing';

import { ResolvePlaylistService } from './resolve-playlist.service';

describe('ResolvePlaylistService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ResolvePlaylistService = TestBed.get(ResolvePlaylistService);
    expect(service).toBeTruthy();
  });
});
