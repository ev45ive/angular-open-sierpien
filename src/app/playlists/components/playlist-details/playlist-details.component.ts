import { Component, OnInit, Input, Output, EventEmitter, HostBinding } from "@angular/core";
import { Playlist } from "src/app/models/Playlist";

@Component({
  selector: "app-playlist-details",
  templateUrl: "./playlist-details.component.html",
  styleUrls: ["./playlist-details.component.scss"]
}) 
export class PlaylistDetailsComponent implements OnInit {

  @HostBinding('class.favorite')
  get isFav(){
    return this.playlist.favorite
  }

  @Input()
  playlist: Playlist;

  @Output()
  edit = new EventEmitter();

  constructor() {}

  ngOnInit() {}
}
