import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  Output,
  EventEmitter
} from "@angular/core";
import { Playlist } from "src/app/models/Playlist";
import { NgForOf, NgForOfContext } from "@angular/common";

NgForOf;
NgForOfContext;

@Component({
  selector: "app-items-list",
  templateUrl: "./items-list.component.html",
  styleUrls: ["./items-list.component.scss"],
  encapsulation: ViewEncapsulation.Emulated
  // inputs:[
  //   'playlists:items',
  // ]
})
export class ItemsListComponent implements OnInit {
  
  hover: Playlist;
  
  @Input("items")
  playlists: Playlist[] = [];
  
  @Input()
  selected: Playlist;

  @Output()
  selectedChange = new EventEmitter<Playlist>();

  select(playlist: Playlist) {
    this.selectedChange.emit(playlist);
  }

  constructor() {}

  // ngFOr  trackBy: mojafunkcja"
  mojafunkcja(index,item){
    // console.log(index,item)
    return item.id
  }

  ngOnInit() {}
}
