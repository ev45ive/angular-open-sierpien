import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Playlist } from "src/app/models/Playlist";

@Component({
  selector: "app-playlist-form",
  templateUrl: "./playlist-form.component.html",
  styleUrls: ["./playlist-form.component.scss"]
})
export class PlaylistFormComponent implements OnInit {
  @Input()
  playlist: Playlist;

  @Output()
  cancel = new EventEmitter();

  @Output()
  save = new EventEmitter();

  submit(formValue) {
    const updated = {
      ...this.playlist,
      ...formValue
    };
    this.save.emit(updated);
  }

  constructor() {}

  ngOnInit() {}
}
