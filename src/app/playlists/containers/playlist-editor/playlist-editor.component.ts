import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { Playlist } from "src/app/models/Playlist";
import { Router, ActivatedRoute } from "@angular/router";
import { PlaylistsService } from "../../services/playlists.service";
import { pluck } from 'rxjs/operators';

@Component({
  selector: "app-playlist-editor",
  templateUrl: "./playlist-editor.component.html",
  styleUrls: ["./playlist-editor.component.scss"]
})
export class PlaylistEditorComponent implements OnInit {
  selected$ = this.route.parent.data.pipe(
    // extract playlist from route data / resolved data
    pluck<{}, Playlist>("playlist")
  );

  constructor(
    private service: PlaylistsService,
    private router: Router, private route: ActivatedRoute) {}

  cancel() {
    this.router.navigate([".."], {
      relativeTo: this.route
    });
  }

  save(draft: Playlist) {
    // Save
    this.service.save(draft);

    this.router.navigate([".."], {
      relativeTo: this.route
    });
  }

  ngOnInit() {}
}
