import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { PlaylistsService } from "../../services/playlists.service";
import { Observable } from "rxjs";
import { Playlist } from "src/app/models/Playlist";
import { pluck } from "rxjs/operators";

@Component({
  selector: "app-selected-playlist",
  templateUrl: "./selected-playlist.component.html",
  styleUrls: ["./selected-playlist.component.scss"]
})
export class SelectedPlaylistComponent implements OnInit {
  selected$ = this.route.data.pipe(
    // extract playlist from route data / resolved data
    pluck<{}, Playlist>("playlist")
  );

  constructor(private router: Router, private route: ActivatedRoute) {}

  edit() {
    this.router.navigate(["./edit"], {
      relativeTo: this.route
    });
  }

  ngOnInit(): void {}
}
