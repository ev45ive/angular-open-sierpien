import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PlaylistsComponent } from './pages/playlists/playlists.component';
import { ResolvePlaylistService } from './service/resolve-playlist.service';
import { SelectedPlaylistComponent } from './containers/selected-playlist/selected-playlist.component';
import { PlaylistEditorComponent } from './containers/playlist-editor/playlist-editor.component';

const routes: Routes = [
  {
    path: "playlists",
    component: PlaylistsComponent
  },
  {
    path: "playlists/:playlist_id",
    component: PlaylistsComponent,
    data: {
      title: "Lubie placki"
    },
    resolve: {
      playlist: ResolvePlaylistService
    },
    children: [
      {
        path: "",
        component: SelectedPlaylistComponent // ng g c playlists/containers/selected-playlist
      },
      {
        path: "edit", //ng g c playlists/containers/playlist-editor
        component: PlaylistEditorComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule {}
