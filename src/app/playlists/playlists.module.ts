import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistsComponent } from './pages/playlists/playlists.component';
import { ItemsListComponent } from './components/items-list/items-list.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';
import { PlaylistFormComponent } from './components/playlist-form/playlist-form.component';

import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { SelectedPlaylistComponent } from './containers/selected-playlist/selected-playlist.component';
import { PlaylistEditorComponent } from './containers/playlist-editor/playlist-editor.component'

@NgModule({
  declarations: [
    PlaylistsComponent,
    ItemsListComponent,
    ListItemComponent,
    PlaylistDetailsComponent,
    PlaylistFormComponent,
    SelectedPlaylistComponent,
    PlaylistEditorComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    PlaylistsRoutingModule,
    SharedModule
  ],
  exports:[
    PlaylistsComponent
  ]
})
export class PlaylistsModule { }
