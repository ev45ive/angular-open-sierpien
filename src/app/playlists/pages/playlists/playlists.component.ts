import { Component, OnInit } from "@angular/core";
import { NgIf } from "@angular/common";
import { Playlist } from "src/app/models/Playlist";
import { PlaylistsService } from "../../services/playlists.service";
import { Router, ActivatedRoute } from "@angular/router";
import { map, switchMap, pluck } from "rxjs/operators";
import { Observable } from "rxjs";

@Component({
  selector: "app-playlists",
  templateUrl: "./playlists.component.html",
  styleUrls: ["./playlists.component.scss"]
})
export class PlaylistsComponent implements OnInit {
  playlists$ = this.service.getPlaylists();
  
  selected$ = this.route.data.pipe(
    // extract playlist from route data / resolved data
    pluck<{}, Playlist>("playlist")
  );

  active

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: PlaylistsService
  ) {
    // router.events.subscribe()

  }

  select(playlist: Playlist) {
    this.router.navigate(["/playlists", playlist.id]);
  }

  ngOnInit() {}
}
