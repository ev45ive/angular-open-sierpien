import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, of } from "rxjs";
import { Playlist } from "src/app/models/Playlist";

@Injectable({
  providedIn: "root"
})
export class PlaylistsService {
  private playlistsStore = new BehaviorSubject<Playlist[]>([
    {
      id: 123,
      name: "Angular PLAYLISTS Hits!",
      color: "#ff00ff",
      favorite: false
    },
    {
      id: 234,
      name: "Angular Top20!",
      color: "#00ffff",
      favorite: false
    },
    {
      id: 345,
      name: "Best Of Angular !",
      color: "#ffff00",
      favorite: false
    }
  ]);
  playlists = this.playlistsStore.asObservable();

  getPlaylists() {
    return this.playlists;
  }

  getPlaylist(id: Playlist["id"]): Observable<Playlist> {
    const playlists = this.playlistsStore.getValue();

    return of(playlists.find(p => p.id == id));
  }

  constructor() {}

  save(draft: Playlist) {
    const playlists = this.playlistsStore.getValue();

    // Find old one on the list
    const index = playlists.findIndex(p => p.id == draft.id);

    // Replace on list
    playlists.splice(index, 1, draft);

    // Emit
    this.playlistsStore.next(playlists);
  }
}
