import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PlaylistsModule } from "./playlists/playlists.module";
import { FormsModule } from "@angular/forms";
import { SharedModule } from './shared/shared.module';
import { DirectivesExamplesComponent } from './directives-examples/directives-examples.component';
import { TabsExamplesComponent } from './tabs-examples/tabs-examples.component';
import { SecurityModule } from './security/security.module';
import { MusicProviderDirective } from './music-provider.directive';
import { HttpClientModule } from '@angular/common/http';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [AppComponent, DirectivesExamplesComponent, TabsExamplesComponent, MusicProviderDirective],
  entryComponents: [AppComponent],
  imports: [
    BrowserModule, 
    PlaylistsModule, 
    FormsModule,
    SharedModule,
    HttpClientModule,
    // MusicSearchModule,
    SecurityModule.forRoot(environment.authConfig),
    AppRoutingModule, 
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [MusicProviderDirective]
})
export class AppModule {
  // constructor(private app: ApplicationRef) {}
  // ngDoBootstrap() {
  //   // getconfigformserver.then( => ...
  //   this.app.bootstrap(AppComponent, "app-root");
  //   // this.app.bootstrap(SidebarComponent, "app-sidebar");
  // }
}
