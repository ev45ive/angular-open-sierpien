import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MusicSearchComponent } from "./pages/music-search/music-search.component";
import { AlbumPageComponent } from "./pages/album-page/album-page.component";

const routes: Routes = [
  {
    path: "",
    component: MusicSearchComponent
  },
  {
    path: "album/:album_id",
    component: AlbumPageComponent
  },
  // {
  //   path: "music/album/:album_id",
  //   component: AlbumPageComponent
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicSearchRoutingModule {}
