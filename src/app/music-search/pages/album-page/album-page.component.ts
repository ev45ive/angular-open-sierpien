import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { map, switchMap } from "rxjs/operators";
import { MusicSearchService } from "../../services/music-search.service";

@Component({
  selector: "app-album-page",
  templateUrl: "./album-page.component.html",
  styleUrls: ["./album-page.component.scss"]
})
export class AlbumPageComponent implements OnInit {
  
  album$ = this.route.paramMap.pipe(
    map(p => p.get("album_id")),
    switchMap(id => this.service.getAlbum(id))
  )

  constructor(
    private service: MusicSearchService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {}

  currentTrack;

  play(track,player){
    this.currentTrack = track;
    setTimeout(() => player.play(),100)
  }
}
