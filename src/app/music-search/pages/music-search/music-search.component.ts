import { Component, OnInit, Inject } from "@angular/core";
import { Album } from "../../../models/Album";
import { MusicSearchService } from "../../services/music-search.service";
import { Subscription, Subject, empty, ConnectableObservable } from "rxjs";
import {
  takeUntil,
  tap,
  catchError,
  multicast,
  refCount,
  share,
  shareReplay
} from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-music-search",
  templateUrl: "./music-search.component.html",
  styleUrls: ["./music-search.component.scss"]
  // viewProviders:[
  //   MusicSearchService
  // ],
  // providers: [MusicSearchService]
})
export class MusicSearchComponent implements OnInit {
  query$ = this.service.query;
  results$ = this.service.getResults().pipe(
    catchError(err => {
      this.message = err.message;
      return empty();
    })
  );
  message = "";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService
  ) {
    // const query = this.route.snapshot.queryParamMap.get("query");
    this.route.queryParamMap.subscribe(paramMap => {
      const query = paramMap.get("query");
      if (query) {
        this.service.search(query);
      }
    });
  }

  search(query: string) {
    this.router.navigate([], {
      queryParams: {
        query
      },
      relativeTo: this.route,
      replaceUrl:true,
      // navigate without chaning url
      // skipLocationChange:true
    });
  }

  ngOnInit() {}
}
