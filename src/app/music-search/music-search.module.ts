import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MusicSearchRoutingModule } from "./music-search-routing.module";
import { MusicSearchComponent } from "./pages/music-search/music-search.component";
import { SearchFormComponent } from "./components/search-form/search-form.component";
import { SearchResultsComponent } from "./components/search-results/search-results.component";
import { AlbumCardComponent } from "./components/album-card/album-card.component";
import { environment } from "../../environments/environment";
import {
  MusicSearchService
} from "./services/music-search.service";
import { MUSIC_API_URL } from "./services/tokens";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from '@angular/forms';
import { SecurityModule } from '../security/security.module';
import { AlbumPageComponent } from './pages/album-page/album-page.component';

@NgModule({
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    SearchResultsComponent,
    AlbumCardComponent,
    AlbumPageComponent
  ],
  imports: [
    CommonModule, 
    SecurityModule.forChild(),
    HttpClientModule, 
    MusicSearchRoutingModule,
    ReactiveFormsModule
  ],
  exports: [MusicSearchComponent],
  providers: [
    MusicSearchService,
    {
      provide: "placki",
      useValue: "malinowe"
    },
    {
      provide: MUSIC_API_URL,
      useValue: environment.api_url
    },
    // {
    //   provide: MusicSearchService,
    //   useFactory: (url,placki) => {
    //     return new MusicSearchService(url)
    //   },
    //   deps:[MUSIC_API_URL,'placki']
    // },
    // {
    //   provide: MusicSearchService,
    //   useClass: MusicSearchService,
    //   deps:[TESTING_API_URL]
    // },
    // {
    //   provide: MusicSearchService,
    //   useClass: MockMusicSearchService
    // },
    // MusicSearchService,
    // {
    //   // Override existing providers
    //   provide: "SUPER_EXPENSIVE_MODULE_SEARCH_SERVICE",
    //   useValue: "MyCustomValue"
    // }
  ]
})
export class MusicSearchModule {}
