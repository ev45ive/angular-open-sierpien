import { Component, OnInit, HostBinding, Input } from "@angular/core";
import { Album } from 'src/app/models/Album';
import { MusicSearchService } from '../../services/music-search.service';

@Component({
  selector: "app-album-card",
  templateUrl: "./album-card.component.html",
  styleUrls: ["./album-card.component.scss"]
})
export class AlbumCardComponent implements OnInit {
  @Input()
  album: Album;

  @HostBinding("class.card")
  cardClass = true;

  constructor(private service:MusicSearchService) {}

  ngOnInit() {}
}
