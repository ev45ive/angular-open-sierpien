import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import {
  FormGroup,
  FormControl,
  ValidatorFn,
  Validators,
  AbstractControl,
  ValidationErrors,
  AsyncValidatorFn
} from "@angular/forms";
import {
  filter,
  distinctUntilChanged,
  debounceTime,
  mapTo,
  withLatestFrom
} from "rxjs/operators";
import { Observable, Observer, PartialObserver, combineLatest } from "rxjs";

const censorValidator = (badword: string): ValidatorFn => (
  control: AbstractControl
): ValidationErrors | null => {
  //
  const hasError =
    "string" == typeof control.value && control.value.includes(badword);

  return hasError ? { censor: { badword: badword } } : null;
};

const asyncCensorValidator: AsyncValidatorFn = control => {
  // return this.http.get('jakisurl',{value}).pipe(map(res=>validationErrors))

  return Observable.create(
    (observer: PartialObserver<ValidationErrors | null>) => {
      const handler = setTimeout(() => {
        const badword = "batman";
        const hasError =
          "string" == typeof control.value && control.value.includes(badword);
        const result = hasError ? { censor: { badword: badword } } : null;

        observer.next(result);
        observer.complete();
      }, 1000);

      return /* onUnsubscribe */ () => {
        clearTimeout(handler);
      };
    }
  );
};

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.scss"]
})
export class SearchFormComponent implements OnInit {
  queryForm = new FormGroup({
    query: new FormControl(
      "",
      [
        Validators.required,
        Validators.minLength(3)
        // censorValidator("batman")
      ],
      [asyncCensorValidator]
    ),
    type: new FormControl("album")
  });

  @Input()
  set query(q) {
    (this.queryForm.get("query") as FormControl).setValue(q, {
      emitEvent: false
    });
  }

  constructor() {
    (window as any).form = this.queryForm;

    const field = this.queryForm.get("query");

    const validChanges = field.statusChanges.pipe(
      filter(status => status === "VALID"),
      mapTo(true)
    );

    const validValue$ = validChanges.pipe(
      withLatestFrom(field.valueChanges, (valid, value) => value)
    );

    validValue$
      .pipe(distinctUntilChanged())
      .subscribe(next => this.search(next));
  }

  ngOnInit() {}

  @Output()
  queryChange = new EventEmitter<string>();

  search(query: string) {
    this.queryChange.emit(query);

    // console.log(query);
  }
}
