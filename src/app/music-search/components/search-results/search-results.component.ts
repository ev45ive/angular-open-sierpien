import { Component, OnInit, Input } from "@angular/core";
import { Album } from "src/app/models/Album";
import { Router } from "@angular/router";

@Component({
  selector: "app-search-results",
  templateUrl: "./search-results.component.html",
  styleUrls: ["./search-results.component.scss"]
})
export class SearchResultsComponent implements OnInit {
  @Input()
  results: Album[];

  constructor(private router: Router) {}

  selectAlbum(album: Album) {
    this.router.navigate(["/music/album", album.id]);
  }

  ngOnInit() {}
}
