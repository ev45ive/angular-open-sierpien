import { Injectable, Inject, EventEmitter, OnDestroy } from "@angular/core";
import { Album, AlbumsResponse } from "src/app/models/Album";

import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { AuthService } from "../../security/auth.service";

import {
  pluck,
  map,
  catchError,
  startWith,
  mergeMap,
  concatMap,
  switchMap,
  exhaustMap
} from "rxjs/operators";
import {
  Observable,
  empty,
  throwError,
  of,
  concat,
  Subject,
  ReplaySubject,
  BehaviorSubject
} from "rxjs";
// import { MusicSearchModule } from '../music-search.module';
import { MUSIC_API_URL } from "./tokens";

@Injectable({
  providedIn: "root"
  // providedIn: MusicSearchModule
})
export class MusicSearchService implements OnDestroy {
  //
  private queryChanges = new BehaviorSubject<string>("batman");
  public query = this.queryChanges.asObservable();

  private albumsChanges = new BehaviorSubject<Album[]>([]);
  public albums = this.albumsChanges.asObservable();

  constructor(
    @Inject(MUSIC_API_URL)
    private api_url: string,
    private http: HttpClient
  ) {
    (window as any).subject = this.albumsChanges;
    console.log("Start service");

    this.queryChanges
      .pipe(
        map(query => ({
          q: query,
          type: "album"
        })),
        // Merge everything as it comes
        // mergeMap(params =>

        // Wait for previous to complete
        // concatMap(params =>

        // Ignore while loading:
        // exhaustMap(params =>

        // Cancel previous if new one
        switchMap(params =>
          this.http.get<AlbumsResponse>(this.api_url, {
            params
          })
        ),
        o => o,
        map(response => response.albums.items)
      )
      .subscribe(albums => this.albumsChanges.next(albums));
  }

  search(query: string) {
    this.queryChanges.next(query);
  }

  getResults() {
    return this.albums;
  }

  ngOnDestroy(): void {
    console.log("End service");
  }

  getAlbum(id: string) {
    return this.http.get(`https://api.spotify.com/v1/albums/${id}`);
  }
}
