// https://developer.spotify.com/documentation/web-api/reference/

export interface Entity {
  id: string;
  name: string;
}

export interface Album extends Entity {
  images: AlbumImage[];
  artists?: Artist;
}

export interface AlbumImage {
  url: string;
}

export interface Artist extends Entity {}

export interface PagingObject<T> {
  items: T[];
  offset?: number;
  total?: number;
}

export interface AlbumsResponse {
  albums: PagingObject<Album>;
}
