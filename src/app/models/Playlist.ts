export interface Playlist {
  id: number | string;
  name: string;
  favorite: boolean;
  /**
   * Color as HEX value
   * example #ff00ff
   */
  color: string;
  // tracks: Array<Track>;
  // tracks: Track[] | null;
  tracks?: Track[];
}

export interface Track {
  id: number;
  name: string;
}

// type Entity = Track | Playlist

//  class Playlist {
//   id: number;
//   name: string;
// }

// interface Album{
//   id: number;
//   name: string;
// }

// const album:Album = playlist
