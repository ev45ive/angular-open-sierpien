import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { AuthService } from "./auth.service";
import { catchError } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class AuthInterceptorService implements HttpInterceptor {
  constructor(private auth: AuthService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // if(!req.url.includes('..spotify.com...')){
    //   return next.handle(req)
    // }

    const authReq = req.clone({
      setHeaders: {
        Authorization: `Bearer ${this.auth.getToken()}`
      } 
    });

    return next.handle(authReq).pipe(
      catchError((error, caught) => {
        if (
          error instanceof HttpErrorResponse && //
          error.status == 401
        ) {
          this.auth.authorize();
        }

        return throwError(new Error(error.error.error.message));
      })
    );
  }
}
