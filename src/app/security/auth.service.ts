import { Injectable } from "@angular/core";
import { HttpParams } from "@angular/common/http";

// https://github.com/manfredsteyer/angular-oauth2-oidc
// HttpClientXsrfModule
// https://auth0.com/

export class AuthConfig {
  auth_url: string;
  client_id: string;
  redirect_uri: string;
  response_type = "token";
}

@Injectable({
  providedIn: "root"
})
export class AuthService {
  token: string | null = null;

  constructor(private config: AuthConfig) {
    const tokenJSON = sessionStorage.getItem("token");
    if (tokenJSON) {
      this.token = JSON.parse(tokenJSON);
    }

    const access_token = new HttpParams({
      fromString: location.hash
    }).get("#access_token");

    if (access_token) {
      location.hash = "";
      this.token = access_token;
      sessionStorage.setItem("token", JSON.stringify(access_token));
    }
  }

  authorize() {
    const { auth_url, response_type, redirect_uri, client_id } = this.config;

    const p = new HttpParams({
      fromObject: {
        client_id,
        redirect_uri,
        response_type
      }
    });

    const url = `${auth_url}?${p.toString()}`;

    location.href = url;
  }

  getToken() {
    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}
