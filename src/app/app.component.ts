import { Component, ChangeDetectorRef, NgZone } from "@angular/core";

@Component({
  selector: "app-root, .placki .malinowy-sos",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "Szkolenie Angular";
}
