import { Directive, Input, ViewContainerRef, TemplateRef } from "@angular/core";

interface UnlessContext {
  $implicit: string;
  index: number;
}

@Directive({
  selector: "[appUnless]"
})
export class UnlessDirective {
  data = "velociraptora ";
  counter = 0;

  @Input()
  set appUnless(hide) {
    if (hide) {
      this.vcr.clear();
      // var viewCache = this.vcr.detach(0)
      // this.vcr.remove(0)
    } else {
      const ctx = {
        $implicit: this.data,
        index: ++this.counter
      };

      this.vcr.createEmbeddedView(this.tpl, ctx, 0);
      
      // vs
      
      // const view = this.tpl.createEmbeddedView(ctx)
      // this.vcr.insert(view,0)
    }
  }

  constructor(
    private vcr: ViewContainerRef,
    private tpl: TemplateRef<UnlessContext>
  ) {
    console.log("unless", vcr, tpl);
  }
}
