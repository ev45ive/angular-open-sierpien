import { Component, OnInit, Input } from "@angular/core";
import { TabComponent } from "../tab/tab.component";

@Component({
  selector: "app-tabs",
  templateUrl: "./tabs.component.html",
  styleUrls: ["./tabs.component.scss"]
})
export class TabsComponent implements OnInit {
  tabs = [];

  register(tab: TabComponent) {
    this.tabs.push(tab);
  }

  @Input()
  set index(index) {
    this.openIndex(index);
  }

  toggle(tab: TabComponent) {
    this.tabs.forEach(t => (t.open = false));
    tab.open = true;
  }

  openIndex(index: number) {
    const tab = this.tabs[index]
    if(tab)
    this.toggle(tab);
  }

  constructor() {}

  ngOnInit() {}
}
