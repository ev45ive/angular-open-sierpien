import {
  Directive,
  ElementRef,
  Attribute,
  Input,
  DoCheck,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  Renderer2,
  HostBinding,
  HostListener
} from "@angular/core";

@Directive({
  selector: "[appHighlight], .placki-highlight"
  // host:{
  //   '[style.color]':'color',
  //   '(mouseenter)':' activate($event) '
  // }
})
export class HighlightDirective
  implements OnInit, DoCheck, OnChanges, OnDestroy {


  @HostBinding("style.color")
  @HostBinding("style.border-color")
  get color(){
    return this.active? this.appHighlight : ''
  }

  @Input("appHighlight")
  appHighlight: string;

  @HostBinding('class.has-highlight')
  active = false
  
  @HostListener("mouseenter", ["$event.x", "$event.y"])
  activate(x: number, y: number) {
    this.active = true
    // this.color = this.appHighlight;
  }
  
  @HostListener("mouseleave")
  deactivate() {
    this.active = false
    // this.color = "";
  }

  constructor(
    private renderer: Renderer2,
    private elem: ElementRef<HTMLElement>
  ) {
    // console.log("constructor");
  }

  ngOnChanges(changes: SimpleChanges) {
    // console.log("ngOnChanges", changes);
  }

  ngOnInit() {
    // console.log("ngOnInit");
  }

  ngDoCheck() {
    // console.log("ngDoCheck");
  }

  ngOnDestroy() {
    // console.log("ngOnDestroy");
  }
}

//   if (changes["color"].currentValue != changes["color"].previousValue) {
//     // this.elem.nativeElement.style.color = this.color;
//     this.renderer.setStyle(this.elem.nativeElement,'color',this.color)
//   }

// this.renderer.listen(this.elem.nativeElement,'mouseenter',()=>{
//   console.log('placki',this.elem)
// })
